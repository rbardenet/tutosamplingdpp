# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 19:07:28 2015

@author: pchainai
"""

# def prepare_environment():
    
# Import necessary libraries, basically random number generators, linear algebra, and plotting tools
import numpy as np 
import numpy.linalg as npl
import numpy.random as npr
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.stats as spst
import itertools as itt
import matplotlib.patches as patches
#import seaborn as sns
#sns.set(style="ticks");

# Import the tools for this particular session
from tools import *
from dpp import *

# Make wide figures
figsize(18,6)

# Change the size of labels on figures
# plt.rc('axes', labelsize=22)
# plt.rc('legend', fontsize=22)

